from django.db import models
from utils.classes import ModelBasica


# Create your models here.
class Materia(ModelBasica):
    titulo = models.CharField(max_length=120)
    descricao = models.CharField(max_length=120)
    requisito = models.ManyToManyField('Materia', related_name='materia_de_requisito', blank=True, null=True)

    def __str__(self):
        return self.titulo


class Aula(ModelBasica):
    titulo = models.CharField(max_length=100)
    descricao = models.TextField()
    materia = models.ForeignKey('Materia', on_delete=models.PROTECT)
    data = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.titulo


class Anexo(ModelBasica):
    arquivo = models.FileField(upload_to='materia/')
    aula = models.ForeignKey('Aula', on_delete=models.CASCADE)

    def __str__(self):
        return self.arquivo.name
