from django.contrib import admin
from .models import *


# Register your models here.
admin.site.register(Materia)
admin.site.register(Aula)
admin.site.register(Anexo)