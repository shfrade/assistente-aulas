from django.db import models
from django.contrib.auth.models import User


class ModelBasica(models.Model):
    criado_por = models.ForeignKey(User, models.CASCADE)
    criado_em = models.DateTimeField(auto_now_add=True)
    modificado_em = models.DateTimeField(auto_now=True)
    inativo = models.BooleanField(default=False)

    class Meta:
        abstract = True
